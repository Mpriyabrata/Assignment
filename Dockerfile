FROM python:2
RUN pip install maclookup
ADD testMacAddress.py /
ENTRYPOINT [ "python", "./testMacAddress.py" ]
